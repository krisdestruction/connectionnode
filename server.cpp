// IO
#include <iostream>

// Math
#include <math.h>

// Sockets
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// Measure Time
#include <ctime>
#include <chrono>

// Threading
#include <thread>

// Header
#include "server.h"

// Port Constants
#define PORT_SIZE 10
#define PORT_OFFSET 1024

int main() {
  // Set Random Port
  srand( time(NULL) );
  int p = rand() % ( PORT_SIZE ) + PORT_OFFSET;
  
  // Start Communications
  std::cout << "Communication node started on port: " << p << std::endl;
  
  // Start Listener Thread
  std::thread t1( listener, p );
  
  // Start Broadcaster Thread
  std::thread t2( pinger, p );
  
  // Join Threads
  t1.join();
  t2.join();
}

void pinger( int p ) {
  // Always poll
  while( true ) {
    // Ping all ports
    for( int i = 0; i < PORT_SIZE; i++ ) {
      int q = i + PORT_OFFSET;
      // Don't ping own port
      if( q != p )
        ping( q );
    }
    
    // Sleep
    std::this_thread::sleep_for( std::chrono::milliseconds( 10 * 1000 ) );
  }
}

void ping( int p ) {
  // Create Socket
  int S = socket( AF_INET, SOCK_STREAM, 0 );
  if( err( S, "Send Socket" ) ) {
    close( S );
    return;
  }
  
  // Set Address Info
  sockaddr_in a;
  a.sin_family = AF_INET;
  a.sin_port = htons( p );
  inet_pton( AF_INET, "127.0.0.1", &a.sin_addr );

  // Connect to address
  int C = connect( S, (sockaddr*) &a, sizeof( a ) );
  if( err( C, "Send Connect", false ) )
    return;
  
  // Create Random Data
  int bSize = 65535;   // 64 kB
  char* b = new char[bSize];
  for( int j = 0; j < bSize; j++ )
    b[j] = 'D';
  
  // Create Meta Packet
  int tSize = 20;
  char* t = new char[tSize];

  // Set Port
  char* tPort = t + 1;
  *reinterpret_cast<int*>(tPort) = p;

  // Set Time
  typedef std::chrono::high_resolution_clock Clock;
  char* tTime = tPort + sizeof( int );
  *reinterpret_cast<Clock::time_point*>(tTime) = Clock::now();
  
  
  // Transmit Bandwidth Packet, 16 packets = 1 MB
  t[0] = 'A';
  write( S, t, tSize );
  for( int i = 0; i < 16 * 100; i++ )
    write( S, b, bSize );
  write( S, "B", 1 );
  
  // Close Socket
  close( S );
}

void listener( int p ) {
  // Set Address Info
  sockaddr_in a;
  a.sin_family = AF_INET;
  a.sin_port = htons( p );
  inet_pton( AF_INET, "127.0.0.1", &a.sin_addr );
  socklen_t size = sizeof( a );
  
  // Create Socket
  int S = socket( AF_INET, SOCK_STREAM, 0 );
  if( err( S, "Receive Socket" ) )
    return;
  
  // Bind
  int B = bind( S, (struct sockaddr *) &a, sizeof( a ) );
  if( err( B, "Receive Bind" ) )
    return;
  
  // Listen
  int L = listen( S, 999 );
  if( err( L, "Receive Listen" ) )
    return;
  
  // Define clock timer
  typedef std::chrono::high_resolution_clock Clock;
  auto t = Clock::now();
  
  // Receive Data
  int bSize = 1024 * 1024;
  char* b = new char[bSize];
  while( true ) {
    // Accept New Connection
    int A = accept( S, (sockaddr*) &a, &size );
    if( err( A, "Accept" ) )
      return;

    // Read Stream
    int n, pFrom;
    Clock::time_point pTime;
    do {
      // Read packet
      n = read( A, b, bSize );
      for( int i = 0; i < n; i++ ) {
        // Determine how to handle data
        switch( b[i] ) {
          case 'A':
            // Parse Meta Packet
            pFrom = *( (int*) ( b + i + 1 ) );
            i += sizeof( int );
            pTime = *( (Clock::time_point*) (b + i + 1 ) );
            std::cout << "Ping(" << pFrom << "): "<< ( (double) std::chrono::duration_cast<std::chrono::nanoseconds> ( Clock::now() - pTime ).count() ) / 1000 / 1000 << " ms" << std::endl;
            i += sizeof( Clock::time_point );
            t = Clock::now();
            break;
          case 'B':
            // End of 100 MB packet
            std::cout << "Bandwidth(" << pFrom << "): " << 100 / ( (double) std::chrono::duration_cast<std::chrono::nanoseconds> ( Clock::now() - t ).count() ) * pow( 10, 9 ) << " MB/s" << std::endl;
            break;
          default:
            break;
        }
      }
    } while( n > 0 );
  }
}

bool err( int i, std::string s, bool output ) {
  // Valid, no error
  if( i >= 0 )
    return false;
  
  // Check if output is needed
  if( output ) {
    // Print where error occured
    std::cout << "Error on: " << s << std::endl;
    // Print error number
    std::cout << "Errno: " << errno << std::endl;
  }
  
  // Return invalid
  return true;
}

bool err( int i, std::string s ) {
  // Default to print error
  return err( i, s, true );
}
